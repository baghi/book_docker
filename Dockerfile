FROM debian:buster-slim
ADD install_dependencies.sh /
RUN /install_dependencies.sh && rm -rf /install_dependencies.sh /var/lib/apt/lists/*
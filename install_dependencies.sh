#!/bin/bash

apt-get update
apt-get -y upgrade

# Install required packages
apt-get -y install \
    texlive \
    texinfo \
    pdf2svg \
    librsvg2-bin \
    pandoc \
    pandoc-citeproc \
    python-pandocfilters \
    m4 \
    html-xml-utils \
    zip \
    unzip \
    gpg \
    wget \
    curl \
    bc

# Use the inkscape appimage and extract it since
# The docker container can't use fuse in unprivileged mode and the
# Repo's inkscape doesn't work with the --export-area-drawing
# Hopefully will be fixed in next releases
cd /opt
curl -OL https://gitlab.com/inkscape/inkscape/-/jobs/artifacts/master/download?job=appimage%3Alinux
mv "download?job=appimage%3Alinux" appimage.zip
unzip appimage.zip
./Inkscape-*-x86_64.AppImage --appimage-extract > /dev/null
rm -rf appimage.zip Inkscape-*-x86_64.AppImage
cd /

# Install texlive packages
tlmgr init-usertree
# I don't know why sometimes tlmgr gives the
# "Remote repository is newer than local (2018 < 2019)"
# Error. Try to use the original repo later.
tlmgr option repository https://ftp.tu-chemnitz.de/pub/tug/historic/systems/texlive/2018/tlnet-final/
tlmgr install stmaryrd enumitem cleveref braket xypic
updmap-sys

# Install nodejs and its packages
curl -fsSL https://deb.nodesource.com/setup_current.x > nodeinst
bash ./nodeinst
rm ./nodeinst
apt-get install -y nodejs
npm init --yes
npm install -g html-minifier
npm install -g svgo
npm install -g stylelint
npm install -g stylelint-config-standard
npm install -g cssnano --save-dev
npm install -g postcss postcss-cli --save-dev

# Installing calibre from the repository also installs a lot of
# unwanted dependencies. We only need the ebook-convert
mkdir calibre && cd calibre
curl "https://github.com/kovidgoyal/calibre/releases/latest" > rawlink
cut -d'"' -f2 rawlink | sed -E "s|$|/calibre-"$(cut -d"/" -f8 rawlink | cut -d'"' -f1 | tr -d "v")"-x86_64.txz|" | sed -E "s|tag|download|g" > link
xargs -n1 curl -OL < link
tar xf $(ls -1 | grep -E ".*-x86_64\.txz")
rm link rawlink $(ls -1 | grep -E ".*-x86_64\.txz")
cd .. && mv calibre /opt

# Clean the retrieved package files
apt-get autoremove
apt-get clean
sync
